$(document).ready(function () {
  $('.slider').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    adaptiveHeight: true
  })

  $('.switch__button').click(function (event) {
    $('.contraindications-list').removeClass('active')
    $('.switch__button').removeClass('active')
    $(this).addClass('active')
    var num = $(this).attr('data-num')
    $('#contraindications-list' + num).addClass('active')
    if (num === '1') {
      $('.contraindications__second').addClass('active')
    } else {
      $('.contraindications__second').removeClass('active')
    }
  })

  function changeSwitch (width) {
    if (width > 830) {
      $('.switch-first').html('#БодиФлекс_с_АлинойШпак')
      $('.switch-second').html('#ОксиСайз_с_АлинойШпак')
    } else {
      $('.switch-first').html('#БодиФлекс')
      $('.switch-second').html('#ОксиСайз')
    }
  }

  var width = $(window).width()

  changeSwitch(width)

  $(window).resize(function () {
    width = $(window).width()
    changeSwitch(width)
  })

  $('.navigation__list_mobile').click(function (event) {
    $('.mobile-navigation').slideToggle(300, function () {
      $(this).removeAttr('style')
    })
  })

  $('.navigation-button').on('click', function () {
    $('.mobile-navigation').slideToggle(300, function () {
      if ($(this).css('display') === 'none') {
        $(this).removeAttr('style')
      }
    })
  })
})
